<?php

// Require this file to enable classes to be loaded automatically

require_once dirname(__FILE__).'/src/SplClassLoader.php';

$autoload = new SplClassLoader('AdamQuaile\\CSVGenerator', dirname(__FILE__).'/src/');
$autoload->register();
