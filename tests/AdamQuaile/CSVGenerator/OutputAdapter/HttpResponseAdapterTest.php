<?php

use Symfony\Component\HttpFoundation\Response;
use AdamQuaile\CSVGenerator\OutputAdapterInterface;
use AdamQuaile\CSVGenerator\OutputAdapter\HttpResponseAdapter;

/**
 * Test that given valid CSV lines, all output sent to
 * Response object is correct.
 */
class HttpResponseAdapterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var HttpResponseAdapter
     */
    private $adapter;

    public function setup()
    {
        $this->adapter = new \AdamQuaile\CSVGenerator\OutputAdapter\HttpResponseAdapter(new Response(), 'test');
    }

    public function testMimeType()
    {
        $csv = $this->setupSampleCSV($this->adapter);
        $this->assertEquals('text/csv', $this->adapter->getResponse()->headers->get('Content-type'));

    }

    public function testContentDisposition()
    {
        $csv = $this->setupSampleCSV($this->adapter);
        $this->assertTrue(1 === preg_match(
            '/attachment; filename=.*/',
            $this->adapter->getResponse()->headers->get('Content-disposition')
        ));
    }

    public function testOutputAsExpected()
    {
        $sampleResponse = <<<CSV
"header","fields"
"value","fields"
"value","fields"
CSV;
        $this->setupSampleCSV($this->adapter);
        $this->assertEquals($sampleResponse, trim($this->adapter->getResponse()->getContent()));
        $this->adapter->close();
    }

    private function setupSampleCSV(OutputAdapterInterface $adapter)
    {
        $csv = new \AdamQuaile\CSVGenerator\CSV($adapter);
        $csv->setHeaders(array('header', 'fields'));
        $csv->addRow(array('value', 'fields'));
        $csv->addRow(array('value', 'fields'));
        $adapter->close();

        return $csv;
    }
}