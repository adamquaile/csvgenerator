<?php

namespace AdamQuaile\CSVGenerator\OutputAdapter;

/**
 * Output as forced download to a browser, using PHP's
 * native header() functions.
 *
 * If using Symfony (or symfony components), it is much preferred
 * to use the HttpResponseAdapter
 */
class HttpDownloadAdapter implements \AdamQuaile\CSVGenerator\OutputAdapterInterface
{

    private $filename;
    private $headersSent = false;

    public function __construct($downloadFileName)
    {
        $this->filename = $downloadFileName.'.csv';
    }

    public function nextRow($rowString)
    {
        if (!$this->headersSent) {
            $this->sendHeaders();
        }
        echo $rowString . "\n";
    }
    private function sendHeaders() {
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.addslashes($this->filename).'"');

        $this->headersSent = true;
    }

    public function close() {}

}