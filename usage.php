<?php

// This is all that's required to load the classes. No need 
// for more `require`s
require_once 'autoload.php';

use AdamQuaile\CSVGenerator\CSV;
use AdamQuaile\CSVGenerator\OutputAdapter\HttpResponseAdapter;

// Interchangeable output adapter..
$adapter = new \AdamQuaile\CSVGenerator\OutputAdapter\HttpDownloadAdapter('download');

// Manipulate CSV
$csv = new CSV($adapter);
$csv->setHeaders(array('one', 'two'));
$csv->addRow(array('three', 'four'));

// And close, in case adapter needs to do anything
// to tidy up, like close file handles, etc..
$csv->close();
