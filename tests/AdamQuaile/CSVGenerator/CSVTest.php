<?php

use AdamQuaile\CSVGenerator\CSV;

/**
 * Testing here the output that's sent to the adapter,
 * not whether the adapter is formatting/exporting the
 * rows properly.
 */
class CSVTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Create new CSV object, with mock adapter interface
     */
    public function setup()
    {
        // Setup brand new test adapters
        $this->stub = $this->getMock('\AdamQuaile\CSVGenerator\OutputAdapterInterface');
        $this->csv = new CSV($this->stub);
    }

    /**
     * Test that the header row is returned first
     */
    public function testHeadersFirstRow()
    {
        // Very basic CSV
        $headers = array('one', 'two', 'three');
        $firstRow = array('a', 'b', 'c');

        // Check the output adapter gets the rows in right order
        $this->stub
            ->expects($this->at(0))
            ->method('nextRow')
            ->with($this->equalTo('"one","two","three"'));
        $this->stub
            ->expects($this->at(1))
            ->method('nextRow')
            ->with($this->equalTo('"a","b","c"'));

        // Add to CSV and close
        $this->csv->setHeaders($headers);
        $this->csv->addRow($firstRow);
        $this->csv->close();

    }

    /**
     * Test the proper escaping (and quoting) of strings
     */
    public function testStringsEscaped()
    {
        // Value and corresponding quoted escaped value. Use
        // providers if this needs to be improved
        $value = "\\'(,\"\\";
        $escaped = '"' . "\\\\'(\\,\\\"\\\\" . '"';

        $this->stub->expects($this->at(1))->method('nextRow')->with($this->equalTo($escaped));

        $this->csv->setHeaders(array('header'));
        $this->csv->addRow(array($value));
        $this->csv->close();
    }

    /**
     * Test that an exception is thrown when adding
     * a row, if we haven't added the headers.
     */
    public function testRequiresHeaderRow()
    {
        $this->setExpectedException('\\LogicException');
        $this->csv->addRow(array('value'));
        $this->csv->close();
    }

}