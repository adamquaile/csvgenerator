<?php

namespace AdamQuaile\CSVGenerator;

/**
 * Objects implementing this get fed CSV lines sequentially
 * via the nextRow() method.
 *
 * The close() method may also be used to close file handles
 * output to browser, do whatever else is required.
 */
interface OutputAdapterInterface
{
    /**
     * Feed the next row to the adapter.
     *
     * @param string $row Escaped, quoted. No terminating newline!
     *
     * @abstract
     * @return mixed
     */
    public function nextRow($row);

    /**
     * Mark CSV as finished, tidy up.
     *
     * @abstract
     * @return mixed
     */
    public function close();
}