<?php

namespace AdamQuaile\CSVGenerator;

/**
 * Generates a CSV file from given values and passes
 * rows sequentially to an output adapter, which may
 * send to Response, save to file, etc..
 */
class CSV
{

    private $headers;
    private $headersSent;
    private $adapter;

    /**
     * Instantiate class with a given adapter, used to format
     * the output via the required method, e.g. file, to Response,
     * etc..
     *
     * @param OutputAdapterInterface $adapter
     */
    public function __construct(OutputAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->headers = array();
        $this->headersSent = false;
    }

    /**
     * Add header row to CSV. Values will be escaped and quoted.
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        $this->output();
    }

    /**
     * Format and send row data to the chosen adapter.
     *
     * Forces sending of header rows, throws an exception if
     * the headers haven't been set with setHeaders()
     *
     * @param array|null $rowData
     * @throws \LogicException
     */
    private function output(Array $rowData=null)
    {

        if (!$this->headersSent) {

            if (empty($this->headers)) {
                throw new \LogicException("No CSV header row given");
            }

            $this->sendRowToAdapter($this->headers);
            $this->headersSent = true;
        }

        if (!is_null($rowData)) {

            $this->sendRowToAdapter($rowData);
        }

    }

    /**
     * Add another row given array of values
     *
     * @param array $rowData
     *
     * @throws \LogicException
     */
    public function addRow($rowData)
    {

        $this->output($rowData);
    }

    /**
     * Mark the CSV as finished, and do any final
     * cleanup required.
     */
    public function close()
    {
        $this->adapter->close();
    }

    /**
     * Send a row to the adapter.
     * @param $rowData
     */
    private function sendRowToAdapter($rowData)
    {
        $this->adapter->nextRow($this->generateRow($rowData));
    }

    /**
     * Escape and comma-separate an array of data for output into a CSV file.
     *
     * @param $rowData
     * @return string
     */
    private function generateRow($rowData)
    {
        $escape = array('"', ',', "\n", '\\');

        $rowString = '';
        foreach ($rowData as $value) {

            $rowString .= '"';
            for ($i=0; $i<strlen($value); $i++) {

                if (in_array($value{$i}, $escape)) {
                    $rowString .= '\\'.$value{$i};
                } else {
                    $rowString .= $value{$i};
                }
            }
            $rowString .= '",';
        }
        $rowString = substr($rowString, 0, strlen($rowString)-1);

        return $rowString;
    }

}

