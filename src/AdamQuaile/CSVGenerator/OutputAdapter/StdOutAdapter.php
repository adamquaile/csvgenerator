<?php

namespace AdamQuaile\CSVGenerator\OutputAdapter;

/**
 * Class to aid printing the generated CSV to standard
 * out, most useful probably for printing to terminal.
 */
class StdOutAdapter implements \AdamQuaile\CSVGenerator\OutputAdapterInterface
{

    /**
     * Feed the next row to the adapter.
     *
     * @param string $row Escaped, quoted. No terminating newline!
     *
     * @return mixed
     */
    public function nextRow($row)
    {
        echo $row."\n";
    }

    /**
     * Nothing required
     *
     * @return mixed
     */
    public function close() {}

}