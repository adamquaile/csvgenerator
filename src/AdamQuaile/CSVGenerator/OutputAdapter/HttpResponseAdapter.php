<?php

namespace AdamQuaile\CSVGenerator\OutputAdapter;

use Symfony\Component\HttpFoundation\Response;

class HttpResponseAdapter implements \AdamQuaile\CSVGenerator\OutputAdapterInterface
{

    private $response;
    private $filename;

    public function __construct(Response $response, $downloadFileName)
    {
        $this->response = $response;
        $this->filename = $downloadFileName.'.csv';
    }

    public function nextRow($rowString)
    {
        $this->response->setContent($this->response->getContent() . $rowString . "\n");
    }

    /**
     * Make sure the correct headers are set.
     * @return mixed|void
     */
    public function close()
    {
        $this->response->headers->set('Content-type', 'text/csv');
        $this->response->headers->set('Content-Disposition', 'attachment; filename="'.addslashes($this->filename).'"');
    }

    public function getResponse()
    {
        return $this->response;
    }


}